import { storage } from '.';

/**
 * 存储数据管理
 * 1.添加新值需要修改俩处:定义常量和添加到集合中
 */
export default class StorageKeychain {
    public static readonly AppletStatus = 'applet_status';
    public static readonly UnionId = 'shenyang_unionid';
    public static readonly OpenId = 'shenyang_openid';  // 小程序openid
    public static readonly UnionOpenId = 'union_openId'; // 官微openid
    public static readonly BindUnionOpenId = 'bind_union_openId'; // 未绑定官微openid，绑定成功后清空
    public static readonly AuthAccessToken = 'auth_access_token';
    public static readonly UUAccessToken = 'uu_access_token';
    public static readonly CurrentLoginMobile = 'shenyang_user_mobile'; // 用户手机号
    public static readonly SelectCarInfo = 'select_car_info'; // {car_id:车牌号, plate_color：车牌颜色}
    public static readonly ActionInfo = 'action_info'; // 操作信息，避免页面跳转使用
    public static readonly WxUserInfo = 'wx_user_info'; // 微信用户信息
    public static readonly WxUserIds = 'wx_user_ids'; // 微信用户ids { ht_user_id: '', uu_user_id: '' }
    public static readonly WxIdentificationInfo = 'wx_identification_info'; // 微信用户认证信息
    public static readonly ReservationCarID = 'reservation_car_id';
    public static readonly ReservationSearchHistory = 'reservation_search_history'; // 预约搜索-历史记录
    public static readonly ReservationPhoneNumber = 'reservation_phone_number';
    public static readonly FindParkingSearchHistory = 'find_parking_search_history'; // 找车位搜索-历史记录
    public static readonly FullScreen = 'full_screen';  // 判断是否是全面屏
    public static readonly LocationAuthorizationState = 'location_authorization_state'; // 定位的授权状态
    public static readonly FreeInspectionPersonInfo = 'free_inspection_person_info'; // 定位的授权状态
    public static readonly IndexFloatingInfo = 'index_floating_info'; // 首页浮窗数据
    public static readonly RoadAssistanceSearchAddress = 'road_assistance_search_address'; // 首页浮窗数据
    public static readonly IsSearchAddressFrom = 'road_search_address_from'; // 首页浮窗数据
    public static readonly MdbyCarInfo = 'mdby_car_info'; // 门店保养首页数据缓存
    public static readonly MdbyBuyerInfo = 'mdby_buyer_info'; // 门店保养首页数据缓存
}

storage.configValidKeys(StorageKeychain);
