import { timer } from 'rxjs';
import mta from 'mta-wechat-analysis';
import { Environment } from '@/environments/environment';

class ModalHelper {

  public readonly cancelTextColor = '#969699';

  public readonly confirmTextColor = '#E94F41';

  public readonly failCommonMessage = '加载失败';

  constructor() {
    Taro.zmyGetModalConfig = () => {
      return {
        cancelTextColor: this.cancelTextColor, // 取消文本颜色
        confirmTextColor: this.confirmTextColor, // 确认文本颜色
        failCommonMessage: this.failCommonMessage, // 失败通用显示信息
      };
    };

    // 加载中模态框
    Taro.zmyLoading = () => {
      Taro.showLoading({
        title: '加载中',
        mask: true,
      });
    };

    // 提示框
    Taro.zmyToast = (msg: string) => {
      Taro.showToast({
        title: msg,
        icon: 'none',
        duration: 2000,
        mask: true,
      });
    };

    // 操作结果模态框
    Taro.zmyShowResult = (modalContent: string, confirmOperation?: any, modalTitle: string = '', sureText: string = '确定') => {
      timer(1).subscribe(() => {
        Taro.showModal({
          title: modalTitle,
          content: modalContent,
          showCancel: false,
          confirmText: sureText,
          confirmColor: this.confirmTextColor,
          success: (res) => {
            if (res.confirm) {
              if (confirmOperation) {
                confirmOperation();
              }
            }
          },
        });
      });
    };

    // 确认操作模态框
    Taro.zmyShowConfirm = (modalContent: string, confirmOperation?: any, cancelOperation?: any, modalTitle: string = '', canceledText: string = '取消', sureText: string = '确定') => {
      Taro.showModal({
        title: modalTitle,
        content: modalContent,
        cancelText: canceledText,
        cancelColor: this.cancelTextColor,
        confirmText: sureText,
        confirmColor: this.confirmTextColor,
        success: (res) => {
          if (res.confirm) {
            if (confirmOperation) {
              confirmOperation();
            }
          } else {
            if (cancelOperation) {
              cancelOperation();
            }
          }
        },
      });
    };
  }
}

class MtaHelper {

  constructor() {
    /**
     * 添加埋点
     * @param {string} eventId 事件id
     * @param {string} remark 备注
     * @param params 事件参数
     */
    Taro.mtaEventStat = (eventId: string,  params: any = {}) => {
      if (Environment.Version as string === 'R') {
        mta.Event.stat(eventId, params);
      }
    };

    /**
     * 初始化埋点
     */
    Taro.mtaInit = () => {
      mta.App.init({
        appID: '500694161',
        eventID: '500694162',
        autoReport: true,
        statParam: true,
        ignoreParams: [],
      });
    };
  }
}

class WxDeviceHelper {

  constructor() {
    // 加载中模态框
    Taro.zmyMakePhoneCall = (phone: string) => {
      if (phone) {
        Taro.makePhoneCall({
          phoneNumber: phone,
          success: () => {
            console.log('成功拨打电话');
          },
        });
      }
    };
  }
}

/**
 * 微信api扩展
 */
export default class WXExtension {

  // @ts-ignore
  private static wxExtension: WXExtension;
  // @ts-ignore
  private modalHelper = new ModalHelper();
  // @ts-ignore
  private mtaHelper = new MtaHelper();
  // @ts-ignore
  private wxDeviceHelper = new WxDeviceHelper();

  public static Init() {
    WXExtension.wxExtension = new WXExtension();
  }
}
