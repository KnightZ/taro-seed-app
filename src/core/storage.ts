class StorageProvider {
    private _validKeys: Set<string> = new Set<string>();

    /**
     * 配置有效Key集合
     * @param dictKeys 多个有效的key集合
     */
    public configValidKeys(...dictKeys: any[]) {
        if (dictKeys) {
            dictKeys.map(keys => {
                Object.keys(keys).map(key => this._validKeys.add(key));
            });
        }
    }

    public set(key: string, value: string): void {
        if (!this._validKeys.has(key)) {
            throw new Error('The key is not existed.');
        }
        Taro.setStorageSync(key, value);
    }

    public get(key: string): string {
        if (!this._validKeys.has(key)) {
            throw new Error('The key is not existed.');
        }
        return Taro.getStorageSync(key);
    }

    public remove(key: string): any {
        if (!this._validKeys.has(key)) {
            throw new Error('The key is not existed.');
        }
        Taro.removeStorageSync(key);
    }

    public clear() {
        Taro.clearStorageSync();
    }
}

const storage = new StorageProvider();
export default storage;
