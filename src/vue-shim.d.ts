// @ts-nocheck
declare namespace Taro {

  interface ZMYModalConfig {
    cancelTextColor: string; // 取消文本颜色
    confirmTextColor: string; // 确认文本颜色
    failCommonMessage: string; // 失败通用显示信息
  }

  /**
   * 获取对话框配置信息
   * @returns {ZMYModalConfig} 配置信息
   */
  function zmyGetModalConfig(): ZMYModalConfig;

  /**
   * 提示模态框
   *
   * @param {string} msg 提示语
   */
  function zmyToast(msg: string);


  /**
   * 拨打电话
   * @param {string} phone
   */
  function zmyMakePhoneCall(phone: string);

  /**
   * 加载中模态框
   */
  function zmyLoading();

  /**
   * 操作结果提示模态框
   * @param {string} modalContent 提示内容
   * @param {*} [confirmOperation] 确定按钮操作
   * @param {string} [modalTitle=''] 标题
   * @param {string} [sureText='确定'] 确定按钮文字
   */
  function zmyShowResult(modalContent: string, confirmCallback?: any, modalTitle: string = '', sureText: string = '确定');

  /**
   * 确认操作模态框
   * @param {string} modalContent 提示内容
   * @param {*} [confirmCallback] 确定按钮操作回调
   * @param {*} [cancelCallback] 取消按钮操作回调
   * @param {string} [modalTitle=''] 标题
   * @param {string} [canceledText='取消'] 取消按钮内容
   * @param {string} [sureText='确定'] 确定按钮内容
   */
  function zmyShowConfirm(modalContent: string, confirmCallback?: any, cancelCallback?: any, modalTitle: string = '', canceledText: string = '取消', sureText: string = '确定');

  /**
   * 添加埋点
   * @param {string} eventId 事件id
   * @param {string} remark 备注
   * @param params 事件参数
   */
  function mtaEventStat(eventId: string, remark: string = '', params: any = {});

  /**
   * 初始化埋点
   */
  function mtaInit();

  function createCanvasContext(instance?: any, self?: any): CanvasContext;
}
