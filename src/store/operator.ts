import { observable, action } from 'mobx';

class OperatorStore {
    @observable counter = 10;
    tempCount = false;
    @action increment() {
        if (!this.tempCount) {
            this.counter++;
        }
        this.tempCount = !this.tempCount;
        console.log(this.counter, this.tempCount);
    }
}

const operatorStore = new OperatorStore();
export default operatorStore;
