/**
 * 环境变量
 * 考虑部分历史原因,约定明明规范:
 * 1.常量类型采用驼峰式
 * 2.域名类型采用大写全拼下划线分割DOMAIN结尾
 */
export class Environment {
  public static readonly AuthClientId = 'a6b59882137a47fc8cda27d871d75afe'; // 慧停passport帐号分配
  public static readonly AuthClientSecret = '8ba89220284d42bd99b8f125f7891314'; // 慧停passport帐号分配
  public static readonly UUClientId = 'b865a5a273f249af85693bb20e1a707b'; // 悠悠passport帐号分配
  public static readonly UUClientSecret = 'fe1f00466c1d4c858ba6abc4dc200809'; // 悠悠passport帐号分配
  public static readonly Version = 'D';
  public static readonly CardCompanyId = 'eae35a606fd111e888060242ac1c0004';
  public static readonly BUNDLE_ID = 'wx5041d139e198b0af';
  public static readonly QQMAP_KEY = 'QRCBZ-K6SC4-L4XUC-D63XX-42C2V-RWFQ7';
  public static readonly QQMAP_SECRET_KEY = '3o1gskrSTMwbKjE08xIYruszgYDwhtq';

  // 服务器域名
  public static readonly UU_WXMP_DOMAIN = 'https://uu-wxmp-d.parkone.cn'; // 主服务域名
  public static readonly RESERVATION_WXMP_DOMAIN = 'https://reservation-wxmp-server-d.parkone.cn'; // 预约域名
  public static readonly UU_PASSPORT_DOMAIN = 'https://passport-t.uucin.com'; // UU Passport域名
  public static readonly AUTH_DOMAIN = 'https://auth-d.parkone.cn'; // 慧停Passport域名
  public static readonly APOLLO_DOMAIN = 'https://apollo-d.parkone.cn'; // 主服务域名
  public static readonly NEWS_DOMAIN = 'https://mx-news-server-d.parkone.cn'; // 停车资讯域名
  public static readonly NINGBO_DOMAIN = 'https://ningbo-card-server-d.parkone.cn';
  public static readonly PAY_PUBLIC_DOMAIN = 'https://pay-d.uucin.com'; // 聚合支付域名
  public static readonly CARLINE_DOMAIN = 'https://carline-server-d.parkone.cn'; // 检车线域名
  public static readonly INSURANCE_DOMAIN = 'https://insurance-server-d.parkone.cn'; // 保险服务域名
  public static readonly STORAGE_DOMAIN = 'https://uustorage-t.uucin.com'; // 存储服务域名
  public static readonly COUPON_DOMAIN = 'https://coupon-service-d.parkone.cn'; // 平台优惠券域名
  public static readonly COUPON_PARKING_DOMAIN = 'https://apollo-d.uucin.com'; // 我的优惠券可使用停车场域名
  public static readonly QQMAp_DOMAIN = 'https://apis.map.qq.com'; // qq地理位置域名

  // 网页域名
  public static readonly DOMAIN = 'https://mdby-server-d.parkone.cn'; // 自身的域名
  public static readonly NEWS_WEB_DOMAIN = 'https://mx-news-d.parkone.cn'; // 停车资讯前端域名
  public static readonly USER_CENTER_MOBILE_DOMAIN = 'https://user-center-mobile-d.parkone.cn'; // 用户中心域名
  public static readonly ACTIVITY_URL = 'https://activity-mobile-d.parkone.cn'; // h5活动中心域名
  public static readonly CARLINE_URL = 'https://carline-mobile-d.parkone.cn'; // h5检车线域名
  public static readonly GAS_URL = 'https://gas-mobile-d.parkone.cn'; // h5加油卡域名
}
